(ns extrack.core
  (:require [extrack.view :as v]
            [net.cgrand.enlive-html :as h]
            [net.cgrand.reload]
            [compojure.route :as route]
            [compojure.handler :as handler]
            [clojure.string :as string]
            [clojure.data.json :as json]
            [clojure.java.jdbc :as j]
            [clj-time.format]
            [clj-time.core :as time]
            [ring.middleware.resource]
            [cemerick.friend :as friend]
            [harbinger.hipaa :as hipaa]
            (cemerick.friend [workflows :as workflows]
                             [credentials :as creds]))
  (:import harbinger.sdk.data.RadExam)
  (:use compojure.core
        [ring.middleware.content-type :only
         (wrap-content-type)]
        [ring.middleware.file :only (wrap-file)]
        [ring.middleware.file-info :only
         (wrap-file-info)]
        [ring.middleware.stacktrace :only
         (wrap-stacktrace)]
        [ring.util.response :only (redirect)]))


(defn log-formatter [id ip rad-exam-ids]
  (hipaa/explicit-logger "ExTrack" "http://localhost:8080/extrack/" ip id "ExTrack" "rad_exams" rad-exam-ids))

(defn hl7-to-name [name]
 (let
     [[last-name first-name middle-name suffix prefix degree]
      (clojure.string/split name #"\^")]
   (clojure.string/trim
    (clojure.string/join \space [prefix first-name middle-name last-name degree]))))


(def dt-input-formatter
  (clj-time.format/formatter (clj-time.core/default-time-zone)
                             "YYYY-MM-dd HH:mm"
                             "YYYY-MM-dd HH:mm:ss"
                             "YYYY-MM-dd HH:mm:ss.SSSS"))

(def db-spec {:subprotocol "postgresql"
              :subname "//localhost/myDatabase"
              :user "postgres"})

(derive ::admin ::user)

(defn get-user [user]
  (j/with-db-connection [conn db-spec]
    (let [{:keys [access_level] :as user-rec} (first (j/query conn ["select username,password,access_level from users where username = ?" user]))]
      (and user-rec
           (assoc user-rec :roles (if (> access_level 1)
                                    #{::admin}
                                    #{::user}))))))

(defn checker [user]
  (= user (:username (get-user user))))

(defn add-user [user pass & [al]]
  (j/with-db-connection [conn db-spec]
    (if (not (get-user user))
      (do
        (j/insert! conn :users {:username user
                                :password (creds/hash-bcrypt pass)
                                :access_level (or al 1)})
        :success)
      :exists)))


(defn transformer [conn exam]
  {:rad_exam_id (.getId exam)
   :name (hl7-to-name (.name (.patient exam)))
   :scanning_protocol (-> exam (.resource) (.modality) (.modality))
   :wait (Math/ceil (/ (Math/ceil (/ (- (.getMillis (org.joda.time.DateTime.))
                                        (.getTime (.patientWaitStart exam)))
                                     1000))
                       60))
   :time (.appointment (.radExamTime exam))
   :sign_in (.checkIn (.radExamTime exam))
   :accession (.accession exam)
   :delay (:delay (first (j/query conn ["select delay from extrack where accession = ? " (.accession exam)])))}
  )


(defn queryfilter [q] (.and q [(.between q ".radExamTime.appointment"
                                        (.toDate
                                         (org.joda.time.DateMidnight.))
                                        (.toDate (org.joda.time.DateTime.)))
                              (.equal q ".site.site" (.literal q "UIC"))
                              (.isNotNull q ".radExamTime.checkIn")
                              (.isNull q ".radExamTime.signIn")
                              (.isNull q ".radExamTime.beginExam")
                              (.notEqual q ".currentStatus.universalEventType.eventType" (.literal q "cancelled"))]))

(defn get-data []
  (j/with-db-connection [conn db-spec]
  (let [em (harbinger.sdk.DataUtils/getEntityManager)
        q (RadExam/createQuery em)
        qf (queryfilter q)
        _  (.where q qf)
        exams (.list q)]
    (set (vec (map (partial transformer conn) exams))))))




(defn index-handler [req]
  {:status  200
   :headers {"X-ssssf" "a ssg"}})

(defroutes site-routes
  (GET "/" req (friend/authenticated
                (if (= \/ (last (:uri req)))
                  (let [id (friend/identity req)
                        {:keys [username]} (friend/current-authentication id)
                        ip (:remote-addr req)
                        data (get-data)
                        rad-exam-ids (map :rad_exam_id data)]
                    (log-formatter username ip rad-exam-ids)
                    (v/index data
                             (friend/authorized? #{::admin}
                                                 id)))
                  (ring.util.response/redirect (str (:uri req) "/")))))
  (PUT "/" {body :body}
       (friend/authenticated
        (let [rows (json/read-json (slurp body))]
          (prn rows)
          (j/with-db-connection [conn db-spec]
            (doseq [{accession :accession time :time :as row} rows]
              (prn accession time row)
              (try
                (let [data (assoc row
                             :time (and time (java.sql.Timestamp. (.getMillis (clj-time.format/parse dt-input-formatter time)))))]
                  (if (seq (j/query conn ["select accession from extrack where accession = ?" accession]))
                    (j/update! conn :extrack data ["accession = ?" accession])
                    (j/insert! conn :extrack data)))
                (catch Exception e
                  (prn (.getNextException e)))))))
        {:status 200}))
  (GET "/login" [] (v/login-page))
  (GET "/update" [] {:status 200
                     :header {"content-type" "text/html"}
                     :body (h/emit* (v/table (get-data)))})
  (GET "/adduser" [] (friend/authorize #{::admin} (v/adduser-page nil)))
  (POST "/adduser" {params :params}
        (friend/authorize #{::admin}
         (let [u (params "username")
               p (params "password")
               al (Integer/parseInt (params "access_level") 10)
               user-added? (add-user u p al)]
           (prn params)
           {:status 200
            :header {"content-type" "text/html"}
            :body (v/adduser-page user-added? u)})))
 (friend/logout (ANY "/logout" request (ring.util.response/redirect "/extrack/")))
  (route/not-found "Page not found"))

(defn wrap-debug [hdr]
  (fn [req]
    (prn req)
    (hdr req)))

(def handler
  (-> #'site-routes
      (ring.middleware.resource/wrap-resource "public")
      (friend/authenticate {:credential-fn (partial creds/bcrypt-credential-fn get-user)
                            :workflows [(workflows/interactive-form)]
                            :unauthorized-handler (fn [req] 
                                                    (ring.util.response/redirect "/extrack/"))})
      (ring.middleware.params/wrap-params)
      (ring.middleware.session/wrap-session)))

(comment

(def qf (.between q ".radExamTime.appointment" (.toDate (org.joda.time.DateMidnight.)) (.toDate (org.joda.time.DateTime.))))
(def qf (.and q [(.between q ".radExamTime.appointment" (.toDate
	   (org.joda.time.DateMidnight.)) (.toDate (org.joda.time.DateTime.)))
	   (.equal q ".site.site" (.literal q "UIC"))]))
(.equal q ".site.site" "UIC")

(.list q)

(remove nil? (map (comp (memfn totalWait) (memfn radExamMetric)) todays)))
