(ns extrack.view
  (:require [net.cgrand.enlive-html :as h]
            [net.cgrand.reload]
            [clojure.string :as string]
            [clojure.data.json :as json]))

(def col-types {:time "dt"}) 
 
(defn col [accession col-name data]
  (let [td-name (string/join \_ ["cell" (name col-name) accession])]
    (h/do->
     (h/content (str data))
     (h/set-attr :id td-name) 
     (h/remove-attr :class)  
     (if (= col-name :delay) 
       (h/set-attr :onclick (str "crazy('"   
                                 td-name "','"
                                 (col-types col-name "default")

                                 "');")) 
       identity))))
  
(def data-cols [:accession :name :scanning_protocol :time :sign_in :wait :delay]) 

(h/defsnippet data-row "table.html" [:#ExamTable :tbody :tr :td.namecol] [row]
  (h/clone-for [col-name data-cols] (col (:accession row) col-name (col-name row))))

(h/defsnippet table "table.html" [:#ExamTable] [rows]
  [:tbody :tr] (h/clone-for [row rows]
                            (h/content (data-row row)))) 

(h/defsnippet login "login.html" [:#LoginBox] [])

(h/defsnippet adduserpanel "adduser.html" [:#AddUserBox] [])

(h/deftemplate adduser-page "index-tmpl.html" [flash & [user-name]]   
  [:body] (h/content (adduserpanel))
  [:#blah] (h/content (case flash
                        :exists (str "User " user-name " exists! Try a different name.")
                        :success (str "User " user-name " added!")
                        :fail "Failure!"
                        "")))

(h/deftemplate login-page "index-tmpl.html" []
  [:body] (h/content (login)))  
    
(h/deftemplate index "index-tmpl.html" [data admin?]     
  [:div#data] (h/content (table data))      
  [:#numbah] (h/content (str (count data)))       
  [:#add] (if admin?    
            identity      
            (h/substitute "")))         
          
           
     
    
        
    
    
          
      
  
  
  
  
  
    
  
