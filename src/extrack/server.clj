(ns extrack.server
  (:require [ring.server.standalone]
            [extrack.core]))

(defn start-server [& [port]]
  (ring.server.standalone/serve #'extrack.core/handler {:port (or port 3000)
                                                        :open-browser? false}))
