--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: data; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE data (
    name text,
    scanning_protocol text,
    "time" timestamp without time zone,
    delay text,
    id integer NOT NULL
);


ALTER TABLE public.data OWNER TO postgres;

--
-- Name: data_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.data_id_seq OWNER TO postgres;

--
-- Name: data_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE data_id_seq OWNED BY data.id;


--
-- Name: extrack; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE extrack (
    name character varying(80),
    scanning_protocol character varying(80),
    "time" timestamp with time zone,
    delay character varying(80),
    id integer NOT NULL,
    wait integer,
    sign_in text,
    accession text
);


ALTER TABLE public.extrack OWNER TO postgres;

--
-- Name: extrack_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE extrack_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.extrack_id_seq OWNER TO postgres;

--
-- Name: extrack_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE extrack_id_seq OWNED BY extrack.id;


--
-- Name: table1; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE table1 (
    colname integer NOT NULL
);


ALTER TABLE public.table1 OWNER TO postgres;

--
-- Name: table1_colname_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE table1_colname_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.table1_colname_seq OWNER TO postgres;

--
-- Name: table1_colname_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE table1_colname_seq OWNED BY table1.colname;


--
-- Name: test; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE test (
    colname integer NOT NULL
);


ALTER TABLE public.test OWNER TO postgres;

--
-- Name: test1; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE test1 (
    a character(4)
);


ALTER TABLE public.test1 OWNER TO postgres;

--
-- Name: test_colname_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE test_colname_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.test_colname_seq OWNER TO postgres;

--
-- Name: test_colname_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE test_colname_seq OWNED BY test.colname;


--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE users (
    username text,
    password text,
    access_level integer,
    id integer NOT NULL,
    CONSTRAINT positive_access_level CHECK ((access_level > 0)),
    CONSTRAINT users_access_level_check CHECK ((access_level < 3))
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: weather; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE weather (
    city character varying(80),
    temp_lo integer,
    temp_hi integer,
    prcp real,
    date date
);


ALTER TABLE public.weather OWNER TO postgres;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY data ALTER COLUMN id SET DEFAULT nextval('data_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY extrack ALTER COLUMN id SET DEFAULT nextval('extrack_id_seq'::regclass);


--
-- Name: colname; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY table1 ALTER COLUMN colname SET DEFAULT nextval('table1_colname_seq'::regclass);


--
-- Name: colname; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY test ALTER COLUMN colname SET DEFAULT nextval('test_colname_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: users_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_username_key UNIQUE (username);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

