doUpdate = true;
timeout = 10000;

function update() {
    if(!doUpdate)
	return false;
    jQuery.ajax("update",
		{type: "GET",
		 processData: false
		})
  .done(function(data) {
      $('#ExamTable').DataTable().destroy();
      $('#ExamTable').replaceWith(data);
      var table = $('#ExamTable').DataTable( {
      stateSave: true});
      var count = table.rows().invalidate().data().length;
      $('#numbah').empty();
      $('#numbah').append(count);
      $('#ExamTable').addClass("hover stripe row-border order-column");
      setTimeout(update, timeout);


    });

    return false;
};


function crazy(x, type) {
    $('button').removeAttr("disabled");
    var elm_id = '#' + x;
    var input_id = x + "_input";
    var elm = $(elm_id);
    var data = elm.text();
    elm.empty();
    elm.append('<input id="'+input_id+'" type="text" value="'+data+'">');
    elm.removeAttr("onclick");
    $('#' + input_id)[0].data_type = type;
    doUpdate = false;
    return false;
}

$(document).ready(function() {

    $('#ExamTable').DataTable({
    stateSave: true});
    $('#ExamTable').addClass("hover stripe row-border order-column");
    $('button').click(function () {
	$('td input').each(function(i, input_elm) {
	    var value = $(input_elm).val();
	    var elm = $(input_elm.parentNode);
	    elm.empty();
	    elm.append(value);
	    elm.attr({"onclick": "crazy('"+elm[0].id+"','"+input_elm.data_type+"');"});
	    
	});
	$('button').attr({"disabled": ""});
	var table_data = $('#ExamTable').DataTable().rows().invalidate().data();	
	var data = _.map(table_data,
			 function(x) {
			     var obj = _.object(["accession",
						 "name",
						 "scanning_protocol",
						 "time",
						 "sign_in",
						 "wait",
						 "delay"],
						_.map(x,function(y) {
						    if(y == '') {
							return null;
						    } else {
							return y;
						    }
						}));
			     obj.wait = parseInt(obj.wait,10);
			     return obj;
			 });
	jQuery.ajax("" ,
		    {type: "PUT" ,
		     data: JSON.stringify(data),
		     contentType: 'application/json',
		     processData: false});
	console.log();
	doUpdate = true;
	setTimeout(update, timeout);
	return false;
    });
    setTimeout(update, timeout);
});
