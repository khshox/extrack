(defproject extrack "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [ring/ring-codec "1.0.0"]
                 [enlive "1.1.5"]
                 [ring "1.3.0"]
                 [org.clojure/data.json "0.2.5"]
                 [org.clojure/java.jdbc "0.3.4"]
                 [compojure "1.1.8"]
                ;; [liberator "0.12.0"]
                [org.postgresql/postgresql "9.3-1102-jdbc41"]
                [com.cemerick/friend "0.2.1"]
                [harbinger-sdk "5747e25"]
                 ]
  :plugins [[lein-ring "0.8.11"]]
  :profiles {:dev {:dependencies [[ring-server "0.3.1"]
                                  ]}
             :sha {:plugins [[lein-sha-version "0.1.1"]]}
             :jndi {:resource-paths ["jndi-resources"]}}
  :ring {:nrepl {:start? true}
         :handler extrack.core/handler         
                                        ; :init extrack.core/init
                                        ; :destroy extrack.core/destroy
           }
  :repositories ^:replace {"really-local" "http://repo.radiology.umm.edu:8081/nexus/content/groups/public"})
